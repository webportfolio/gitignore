# Global .gitignore

#### Install
```sh
$ cd ~
$ git clone git@bitbucket.org:webportfolio/gitignore.git
$ git config -l
```

#### Build new `.gitignore_output`
```sh
for i in `\ls *gitignore`; do \
OUT=.gitignore_output; \
echo '### '$i | cut -d'.' -f 1 >> ${OUT}; \
cat $i >> ${OUT}; \
echo '' >> ${OUT}; \
done
```
#### Enjoy!